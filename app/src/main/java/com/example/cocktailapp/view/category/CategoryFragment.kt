package com.example.cocktailapp.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.cocktailapp.databinding.FragmentCategoryBinding
import com.example.cocktailapp.viewmodel.CategoryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryAdapter by lazy { CategoryAdapter(::categoryClicked) }

    private val categoryViewModel by viewModel<CategoryViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryViewModel.categoryState.observe(viewLifecycleOwner) { state ->
            binding.rvCategoryList.adapter = categoryAdapter.apply {
                loadCategory(state.categories)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun categoryClicked(item: String) {
        val drinkList = CategoryFragmentDirections.goToDrinkListFragment(item)
        findNavController().navigate(drinkList)
    }

}