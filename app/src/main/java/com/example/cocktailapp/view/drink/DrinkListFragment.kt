package com.example.cocktailapp.view.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.cocktailapp.databinding.FragmentDrinkListBinding
import com.example.cocktailapp.viewmodel.DrinkListViewModel

class DrinkListFragment : Fragment() {

    private var _binding: FragmentDrinkListBinding? = null
    private val binding get() = _binding!!

    private val drinkListAdapter by lazy { DrinkListAdapter(::drinkClicked) }
    private val drinkListViewModel by viewModels<DrinkListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        drinkListViewModel.drinkListState.observe(viewLifecycleOwner) { state ->
            binding.rvDrinkList.adapter = drinkListAdapter.apply {
                loadDrinkList(state.drinks)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun drinkClicked(id: Int) {
        val drinkDetail = DrinkListFragmentDirections.goToDrinkDetailFragment(id)
        findNavController().navigate(drinkDetail)
    }

}