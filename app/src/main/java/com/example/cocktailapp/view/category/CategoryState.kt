package com.example.cocktailapp.view.category

import com.example.cocktailapp.model.local.entity.Category

data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList(),
    val errorMsg: String? = null
)