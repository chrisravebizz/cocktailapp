package com.example.cocktailapp.view.category

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailapp.databinding.ItemListBinding
import com.example.cocktailapp.model.local.entity.Category

class CategoryAdapter(
    private val drinkList: (String) -> Unit
) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var category = mutableListOf<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoryViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val categoryData = category[position]
        holder.bindCategory(categoryData)
        holder.loadDrinkList().setOnClickListener {
            drinkList(categoryData.strCategory)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }

    fun loadCategory(categoryItem: List<Category>) {
        this.category = categoryItem.toMutableList()
        notifyDataSetChanged()
    }

    class CategoryViewHolder(
        private val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindCategory(category: Category) {
            binding.tvTitle.text = category.strCategory
        }

        fun loadDrinkList(): TextView {
            return binding.tvTitle
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> CategoryViewHolder(binding) }
        }
    }
}