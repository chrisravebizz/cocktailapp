package com.example.cocktailapp.view.drinkdetail

import com.example.cocktailapp.model.local.entity.DrinkList

data class DrinkDetailState(
    val isLoading: Boolean = false,
    val detail: DrinkList = DrinkList(strDrink = "Title", strDrinkThumb = "Image"),
    val errorMsg: String? = null
)
