package com.example.cocktailapp.view.drink

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailapp.databinding.ItemDrinkListBinding
import com.example.cocktailapp.model.local.entity.DrinkList

class DrinkListAdapter(private val drinkDetail: (Int) -> Unit) :
    RecyclerView.Adapter<DrinkListAdapter.DrinkListViewHolder>() {

    private var drinkList = mutableListOf<DrinkList>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DrinkListViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: DrinkListViewHolder, position: Int) {
        val drinks = drinkList[position]
        holder.bindDrinks(drinks)
        holder.loadDrink().setOnClickListener {
            drinkDetail(drinks.id)
        }
    }

    override fun getItemCount(): Int {
        return drinkList.size
    }

    fun loadDrinkList(item: List<DrinkList>) {
        this.drinkList = item.toMutableList()
        notifyDataSetChanged()
    }

    class DrinkListViewHolder(
        private val binding: ItemDrinkListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindDrinks(drink: DrinkList) {
            binding.tvDringName.text = drink.strDrink
            binding.ivThumb.load(drink.strDrinkThumb)
        }

        fun loadDrink(): ImageView {
            return binding.ivThumb
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinkListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinkListViewHolder(it) }
        }
    }
}