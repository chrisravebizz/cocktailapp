package com.example.cocktailapp.view.drink

import com.example.cocktailapp.model.local.entity.DrinkList

data class DrinkListState(
    val isLoading: Boolean = false,
    val drinks: List<DrinkList> = emptyList(),
    val errorMsg: String? = null
)