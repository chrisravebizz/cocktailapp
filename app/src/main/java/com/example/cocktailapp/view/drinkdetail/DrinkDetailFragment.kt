package com.example.cocktailapp.view.drinkdetail

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import coil.load
import com.example.cocktailapp.databinding.FragmentDrinkDetailBinding
import com.example.cocktailapp.viewmodel.DrinkDetailViewModel

class DrinkDetailFragment : Fragment() {

    private var _binding: FragmentDrinkDetailBinding? = null
    private val binding get() = _binding!!

    private val drinkDetailViewModel by viewModels<DrinkDetailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        drinkDetailViewModel.drinkDetailState.observe(viewLifecycleOwner) { state ->
            with(binding) {
                tvTitle.text = state.detail.strDrink
                ivThumb.load(state.detail.strDrinkThumb)
                tvDescription.movementMethod = ScrollingMovementMethod()
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}