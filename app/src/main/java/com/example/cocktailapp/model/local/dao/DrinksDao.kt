package com.example.cocktailapp.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.cocktailapp.model.local.entity.Category
import com.example.cocktailapp.model.local.entity.DrinkList

@Dao
interface DrinksDao {

    @Query("SELECT * FROM category")
    suspend fun getAllCategories(): List<Category>

    @Insert
    suspend fun insertCategory(category: List<Category>)

    @Query("SELECT * FROM drinkList")
    suspend fun getAllDrinks(): List<DrinkList>

    @Insert
    suspend fun insertDrinkList(drinkList: List<DrinkList>)

    @Query("SELECT * FROM drinklist WHERE id=:id")
    suspend fun getDrinkDetail(id: Int): DrinkList

}