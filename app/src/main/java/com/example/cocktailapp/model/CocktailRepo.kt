package com.example.cocktailapp.model

import com.example.cocktailapp.model.local.dao.DrinksDao
import com.example.cocktailapp.model.local.entity.Category
import com.example.cocktailapp.model.local.entity.DrinkList
import com.example.cocktailapp.model.remote.CocktailApi
import com.example.cocktailapp.model.response.CategoryDTO
import com.example.cocktailapp.model.response.DrinkListDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class CocktailRepo {

    private val service: CocktailApi by inject(CocktailApi::class.java)
    private val cocktailService by lazy { service }
    private val drinkDatabase: DrinksDao by inject(DrinksDao::class.java)

    // run our methods on another thread
//    suspend fun getCategories() = withContext(Dispatchers.IO) {
//        cocktailService.getCategories().categoryItem.map { it.strCategory }
//    }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        val cachedCategory: List<Category> = drinkDatabase.getAllCategories()

        return@withContext cachedCategory.ifEmpty {
            val categoryData: CategoryDTO = cocktailService.getCategories()
            val category: List<Category> = categoryData.categoryItem.map {
                Category(strCategory = it.strCategory)
            }
            drinkDatabase.insertCategory(category)
            return@ifEmpty category
        }
    }

//    suspend fun getDrinksList(category: String) = withContext(Dispatchers.IO) {
//        cocktailService.getDrinksList(category).drinks
//    }

    suspend fun getDrinksList(category: String) = withContext(Dispatchers.IO) {
        val cachedDrinkList: List<DrinkList> = drinkDatabase.getAllDrinks()

        return@withContext cachedDrinkList.ifEmpty {
            val drinkListData: DrinkListDTO = cocktailService.getDrinksList(category)
            val drinkList: List<DrinkList> = drinkListData.drinks.map {
                DrinkList(strDrink = it.strDrink, strDrinkThumb = it.strDrinkThumb)
            }
            drinkDatabase.insertDrinkList(drinkList)
            return@ifEmpty drinkList
        }
    }

//    suspend fun getDrinkDetail(drinkItem: Int): Drink = withContext(Dispatchers.IO) {
//        cocktailService.getDrinkDetail(drinkItem).drinks.map {
//            Drink(
//                title = it.strDrink,
//                thumbNail = it.strDrinkThumb,
//                instructions = it.strInstructions
//            )
//        }[0]
//    }

    suspend fun getDrinkDetail(drinkItem: Int) = withContext(Dispatchers.IO) {
        val drink = drinkDatabase.getDrinkDetail(drinkItem)
        return@withContext drink
    }

}