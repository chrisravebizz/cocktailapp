package com.example.cocktailapp.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Drink(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String = "Title",
    val thumbNail: String = "Image",
    val instructions: String = "Description"
)
