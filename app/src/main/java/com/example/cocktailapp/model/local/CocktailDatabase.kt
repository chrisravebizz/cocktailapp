package com.example.cocktailapp.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.cocktailapp.model.local.dao.DrinksDao
import com.example.cocktailapp.model.local.entity.Category
import com.example.cocktailapp.model.local.entity.DrinkList

@Database(entities = [Category::class, DrinkList::class], version = 2)
abstract class CocktailDatabase : RoomDatabase() {

    abstract fun drinksDao(): DrinksDao

    companion object {

        private const val DATABASE_NAME = "cocktails.db"

        // For singleton instantiation
        @Volatile
        private var instance: CocktailDatabase? = null

        fun getInstance(context: Context): CocktailDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        // Create and pre-populate the database. See this article for more details:
        private fun buildDatabase(context: Context): CocktailDatabase {
            return Room.databaseBuilder(context, CocktailDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }

}