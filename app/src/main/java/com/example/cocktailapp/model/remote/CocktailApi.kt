package com.example.cocktailapp.model.remote

import com.example.cocktailapp.model.response.CategoryDTO
import com.example.cocktailapp.model.response.DrinkDetailsDTO
import com.example.cocktailapp.model.response.DrinkListDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailApi {

    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"

        fun getInstance() : CocktailApi {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query("c") category: String = "list"): CategoryDTO

    @GET("/api/json/v1/1/filter.php")
    suspend fun getDrinksList(@Query("c") drinks: String = "Ordinary_Drink"): DrinkListDTO

    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkDetail(@Query("i") detail: Int = 11007) : DrinkDetailsDTO

}