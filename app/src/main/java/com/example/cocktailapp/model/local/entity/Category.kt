package com.example.cocktailapp.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val strCategory: String = "Category"
)