package com.example.cocktailapp.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DrinkList(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val strDrink: String = "Name",
    val strDrinkThumb: String = "Image"
)
