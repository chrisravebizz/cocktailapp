package com.example.cocktailapp.model.response

import com.google.gson.annotations.SerializedName

data class CategoryDTO(
    @SerializedName("drinks")
    val categoryItem: List<Category>
) {
    data class Category(
        val strCategory: String,
    )
}