package com.example.cocktailapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.cocktailapp.domain.GetCategoryUseCase
import com.example.cocktailapp.model.local.entity.Category
import com.example.cocktailapp.view.category.CategoryState
import org.koin.java.KoinJavaComponent.inject

class CategoryViewModel : ViewModel() {

    private val getCategoryUseCase: GetCategoryUseCase by inject(GetCategoryUseCase::class.java)

    val categoryState = liveData {
        emit(CategoryState(isLoading = true))
        val categoryListResult: Result<List<Category>> = getCategoryUseCase()
        val state = CategoryState(
            categories = categoryListResult.getOrNull() ?: emptyList(),
            errorMsg = categoryListResult.exceptionOrNull()?.message
        )
        emit(state)
    }

}