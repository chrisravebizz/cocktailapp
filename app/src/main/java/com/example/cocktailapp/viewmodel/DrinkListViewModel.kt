package com.example.cocktailapp.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.cocktailapp.domain.GetDrinkListUseCase
import com.example.cocktailapp.view.drink.DrinkListState
import org.koin.java.KoinJavaComponent.inject

class DrinkListViewModel(
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val getDrinkListUseCase: GetDrinkListUseCase by inject(GetDrinkListUseCase::class.java)
    private val drinkId: String = savedStateHandle["category"]!!

    val drinkListState = liveData {
        emit(DrinkListState(isLoading = true))
        val drinkListResult = getDrinkListUseCase(drinkId)
//        val state = DrinkListState(
//            drinks = drinkListResult.getOrNull() ?: emptyList(),
//            errorMsg = drinkListResult.exceptionOrNull()?.message
//        )
        emit(DrinkListState(drinks = drinkListResult))
    }

}