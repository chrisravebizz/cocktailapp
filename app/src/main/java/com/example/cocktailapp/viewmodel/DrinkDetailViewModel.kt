package com.example.cocktailapp.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.cocktailapp.domain.GetDrinkDetailUseCase
import com.example.cocktailapp.view.drinkdetail.DrinkDetailState
import org.koin.java.KoinJavaComponent.inject

class DrinkDetailViewModel(
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val getDrinkListUseCase: GetDrinkDetailUseCase by inject(GetDrinkDetailUseCase::class.java)
    private val args: Int = savedStateHandle["drink"]!!

    val drinkDetailState = liveData {
        emit(DrinkDetailState(isLoading = true))
        val drinkDetailResult = getDrinkListUseCase(args)
        emit(DrinkDetailState(detail = drinkDetailResult))
    }

}