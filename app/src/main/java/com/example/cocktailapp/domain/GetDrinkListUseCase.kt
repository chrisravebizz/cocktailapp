package com.example.cocktailapp.domain

import com.example.cocktailapp.model.CocktailRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetDrinkListUseCase {

    private val cocktailRepo: CocktailRepo by inject(CocktailRepo::class.java)

    suspend operator fun invoke(list: String) = withContext(Dispatchers.Default) {
        cocktailRepo.getDrinksList(list)
    }

//    suspend operator fun invoke(list: String): Result<List<DrinkList>> =
//        withContext(Dispatchers.Default) {
//
//            return@withContext try {
//                val drinks = cocktailRepo.getDrinksList(list)
//                Result.success(drinks)
//            } catch (ex: Exception) {
//                Result.failure(ex)
//            }
//        }
}