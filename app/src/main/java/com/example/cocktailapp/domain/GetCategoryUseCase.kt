package com.example.cocktailapp.domain

import com.example.cocktailapp.model.CocktailRepo
import com.example.cocktailapp.model.local.entity.Category
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetCategoryUseCase {
    private val repo: CocktailRepo by inject(CocktailRepo::class.java)

    suspend operator fun invoke(): Result<List<Category>> = withContext(Dispatchers.Default) {

        return@withContext try {
            val categoryResponse = repo.getCategories()
            Result.success(categoryResponse)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }

}