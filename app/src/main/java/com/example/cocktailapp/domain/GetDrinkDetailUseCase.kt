package com.example.cocktailapp.domain

import com.example.cocktailapp.model.CocktailRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent.inject

class GetDrinkDetailUseCase {

    private val cocktailRepo: CocktailRepo by inject(CocktailRepo::class.java)

    suspend operator fun invoke(id: Int) = withContext(Dispatchers.Default) {
        cocktailRepo.getDrinkDetail(id)
    }
}