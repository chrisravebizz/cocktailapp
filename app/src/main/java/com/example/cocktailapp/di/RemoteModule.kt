//package com.example.cocktailapp.di
//
//import com.example.cocktailapp.model.remote.CocktailApi
//import dagger.Module
//import dagger.Provides
//import dagger.hilt.InstallIn
//import dagger.hilt.components.SingletonComponent
//import dagger.hilt.migration.DisableInstallInCheck
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//import retrofit2.create
//import javax.inject.Singleton
//
//@Module
//@InstallIn(SingletonComponent::class)
//object RemoteModule {
//
//    @Provides
//    @Singleton
//    fun providesRetrofit(): Retrofit = Retrofit.Builder()
//        .baseUrl("https://www.thecocktaildb.com")
//        .addConverterFactory(GsonConverterFactory.create())
//        .build()
//
//    @Provides
//    @Singleton
//    fun providesCocktailService(retrofit: Retrofit): CocktailApi = retrofit.create()
//
//}