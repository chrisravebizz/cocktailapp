package com.example.cocktailapp.di

import com.example.cocktailapp.domain.GetCategoryUseCase
import com.example.cocktailapp.domain.GetDrinkDetailUseCase
import com.example.cocktailapp.domain.GetDrinkListUseCase
import com.example.cocktailapp.model.CocktailRepo
import com.example.cocktailapp.model.local.CocktailDatabase
import com.example.cocktailapp.model.remote.CocktailApi
import com.example.cocktailapp.viewmodel.CategoryViewModel
import com.example.cocktailapp.viewmodel.DrinkDetailViewModel
import com.example.cocktailapp.viewmodel.DrinkListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinModule {

    val appModule = module {

        single { CocktailApi.getInstance() }
        single { CocktailDatabase.getInstance(get()) }
        single { get<CocktailDatabase>().drinksDao() }
        single { CocktailRepo() }

        single { GetCategoryUseCase() }
        single { GetDrinkListUseCase() }
        single { GetDrinkDetailUseCase() }

        viewModel { CategoryViewModel() }
        viewModel { DrinkListViewModel(get()) }
        viewModel { DrinkDetailViewModel(get()) }
    }
}