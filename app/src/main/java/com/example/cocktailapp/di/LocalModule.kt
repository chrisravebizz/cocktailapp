//package com.example.cocktailapp.di
//
//import android.content.Context
//import com.example.cocktailapp.model.local.CocktailDatabase
//import dagger.Module
//import dagger.Provides
//import dagger.hilt.InstallIn
//import dagger.hilt.android.qualifiers.ApplicationContext
//import dagger.hilt.components.SingletonComponent
//import javax.inject.Singleton
//
//@Module
//@InstallIn(SingletonComponent::class)
//object LocalModule {
//
//    @Provides
//    @Singleton
//    fun providesCategoryDatabase(@ApplicationContext context: Context): CocktailDatabase =
//        CocktailDatabase.getInstance(context)
//}