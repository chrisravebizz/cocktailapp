plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-kapt")
}

android {
    compileSdk = 32

    defaultConfig {
        applicationId = "com.example.cocktailapp"
        minSdk = 23
        targetSdk = 32
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.7.0")
    implementation("androidx.appcompat:appcompat:1.4.1")
    implementation("com.google.android.material:material:1.5.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.3")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
    // Navigation
    val nav_version = "2.4.1"
    implementation("androidx.navigation:navigation-fragment-ktx:$nav_version")
    implementation("androidx.navigation:navigation-ui-ktx:$nav_version")
    // Retrofit (Network)
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    // Gson (Parsing)
    implementation("com.google.code.gson:gson:2.9.0")
    // Retrofit Gson Adapter
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    // LiveData
    val lifecycle_version = "2.5.0-alpha05"
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version")
    // Coil
    implementation("io.coil-kt:coil:2.0.0-rc02")
    // Hilt
//    implementation "com.google.dagger:hilt-android:2.38.1"
//    kapt "com.google.dagger:hilt-compiler:2.38.1"
    // Room library for SQLite database
    val room_version = "2.4.2"
    implementation("androidx.room:room-runtime:$room_version")
    implementation("androidx.room:room-ktx:$room_version") // Coroutine Support
    kapt("androidx.room:room-compiler:$room_version")

    // ==== Koin ===
    val koin_version = "3.2.0-beta-1"
    // Koin main features for Android
    implementation("io.insert-koin:koin-android:$koin_version")
    // Navigation Graph
    implementation("io.insert-koin:koin-androidx-navigation:$koin_version")
    // Koin Test features
    testImplementation("io.insert-koin:koin-test:$koin_version")
}